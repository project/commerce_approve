<?php

namespace Drupal\commerce_approve\Plugin\views\field;

use Drupal\commerce_product\Entity\Product;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Drupal\Component\Utility\Html;
use Drupal\commerce_order\Entity\OrderItemInterface;

/**
 * Defines a form element for approving order items.
 *
 * @ViewsField("commerce_approve")
 */
class Approve extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['required'] = ['default' => FALSE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Required on cart page'),
      '#default_value' => $this->options['required'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('commerce_approve.settings');
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }
    // Disable HTML5 Validation. Need to be added in configuration.
    $form['#attributes']['novalidate'] = TRUE;

    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $orderItem = $this->getEntity($row);

      $requiresApproval = $this->requiresApproval($orderItem);
      if (!$requiresApproval) {
        continue;
      }
      $ajax_wrapper = Html::getUniqueId('item-approve' . $row_index);

      $form[$this->options['id']][$row_index] = [
        '#prefix' => '<div id="' . $ajax_wrapper . '">',
        '#suffix' => '</div>',
        '#orderItemID' => $orderItem->id(),
        '#op' => $this->options['id'],
        '#title' => $requiresApproval['text'] ??  t('I have verified this product is correct'),
        '#type' => 'checkbox',
        '#required' => $this->options['required'],
        '#row_index' => $row_index,
      ];
    }
  }

  /**
   * Validate handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormValidate(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();

    if (isset($triggering_element['#submit']) && in_array('commerce_checkout_order_item_views_form_submit', $triggering_element['#submit'])) {
      return;
    }
    // Temporarily store all form errors.
    $form_errors = $form_state->getErrors();

    // Clear the form errors.
    $form_state->clearErrors();

    foreach ($form_errors as $key => $error) {
      if (strpos($key, 'commerce_approve') !== FALSE) {
        unset($form_errors[$key]);
      }
    }
    // Now loop through and re-apply the remaining form error messages.
    foreach ($form_errors as $name => $error_message) {
      $form_state->setErrorByName($name, $error_message);
    }
  }

  /**
   * Submit handler for the views form, adds flag to order item data.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('commerce_approve', []);
    $storage = \Drupal::entityTypeManager()->getStorage('commerce_order_item');
    foreach ($values as $key => $value) {
      $orderItemID = $form['commerce_approve'][$key]['#orderItemID'];
      if ($entity = $storage->load($orderItemID)) {
        $entity->setData('approved', TRUE)->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

  /**
   * Finds fields referencing terms and check if it requires an approval.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $orderItem
   *   Order item to check.
   *
   * @return bool|array
   *   Array with values if it requires manual approval, FALSE otherwise.
   */
  public function requiresApproval(OrderItemInterface $orderItem) {
    /** @var \Drupal\commerce\PurchasableEntityInterface */
    $purchasedEntity = $orderItem->getPurchasedEntity();
    foreach ($purchasedEntity->referencedEntities() as $referencedEntity) {
      if ($referencedEntity->getEntityTypeId() == 'commerce_product'){
        /** @var \Drupal\commerce_product\Entity\Product $product */
        $product = $referencedEntity;
        foreach ($product->referencedEntities() as $productReferencedEntity) {
          if ($productReferencedEntity->getEntityTypeId() == 'taxonomy_term' &&
            $productReferencedEntity->hasField('field_require_approval')) {
            if ($productReferencedEntity->get('field_require_approval')->getValue()[0]['value'] == TRUE) {
              if (!$productReferencedEntity->get('field_require_approval_text')->isEmpty()) {
                return [
                  'text' => $productReferencedEntity->get('field_require_approval_text')->getValue()[0]['value'],
                ];
              }
              return TRUE;
            }
          }
        }
      }
    }
    return FALSE;
  }

}
